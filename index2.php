<!DOCTYPE html>
<html>
  <head>
     <title>Birdtwitcher Homepage</title>
	 
	 
	 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link href="styles/css/styles.css" rel="stylesheet"/>	 
  </head>
  <body>
       <header class="navbar-inverse">
	   <div class="container ">
	   <nav class="navbar navbar-inverse" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Welcome to Birdtwitcher</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Login</a></li>
		<li><a href="#">Register</a></li>
      </ul>
    <!-- /.navbar-collapse -->   
  </div><!-- /.container-fluid -->
 
 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-">
        <li class="active"><a href="#">Homepage</a></li>
        <li><a href="#">Another Page</a></li>
		<li><a href="#">Another Page</a></li>
		<li><a href="#">Another Page</a></li>
      </ul>
	</div>

</nav>
</div>
</header>

<div class="container">
<div class="row">
 <div class="thebox">
 <h1> Welcome to Birdtwitcher!</h1>
 </BR>
 This site is made specifically for bird watching. You can feel free to register 
 on the site and become a member, post your sightings or search for sightings posted by others.
 </br>
 </br>
 If you do not want to register you can still <a href="#">Search</a> for sightings that have been posted.
 <div class="image">
 </br>
<img width="750" height="450" src="images/alanhinchliffe-starling.jpg">
 </div>
 </div>
 </div>
 </div>

  
 
  
  
  
  
  <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  </body>
  </html>
